class User < ActiveRecord::Base
  rolify # :before_add => :remove_current_roles
  
  before_create :remove_tenant_if_company_name_not_provided, :create_the_tenant_if_company_name_provided
  before_save :invitee_company_take_precednece_if_user_is_invited
  after_create :assign_moderator_role_if_owner_of_company
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :invitable

  # Setup accessible (or protected) attributes for your model
  #attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me

  has_one :owned_tenant_company, :class_name => 'TenantCompany', :foreign_key => :owner_id
  has_one :tenant_user
  has_one :tenant_company, :through => :tenant_user

  attr_accessible :contact_detail_attributes, :phone_attributes, :fax_attributes, :emails_attributes, :im_attributes, :role_ids
  attr_accessible :address_detail_attributes, :addresses_attributes, :role_name, :owned_tenant_company_name, :tenant_user_attributes

  has_many :contact_details, :as => :contactable
  
  has_many :phone, :as => :contactable
  has_many :fax, :as => :contactable
  has_many :emails, :as => :contactable
  has_many :im, :as => :contactable
  has_many :addresses, :as => :contactable

  has_many :jobs, :as => :customer


  accepts_nested_attributes_for :phone, :fax, :emails, :addresses, :im
  accepts_nested_attributes_for :contact_details, :tenant_company


  def can_create_new_job? 
    # for now if you have a company you can create a job
    self.belongs_to_a_company?
  end
  def is_invited_and_registering_first_time
    !self.invited_by.blank? && self.sign_in_count == 0
  end

  def invitee_company_take_precednece_if_user_is_invited
    # is this guy invited to begin with and thsi is his first time registering?
    if self.is_invited_and_registering_first_time then
      # he is invited so it means if he has entered the name of any tenant company it shoudl be nullified
      self.owned_tenant_company_name = nil
    end
  end

  def belongs_to_a_company?
    self.tenant_company!= nil
  end

  def current_tenant_company_name
    if self.tenant_company!= nil then
      self.tenant_company.name
    else
      'Etyme'
    end
  end

  def actual_current_tenant_company_name
    if self.tenant_company!= nil then
      self.tenant_company.name
    else
      false
    end
  end


  def owned_tenant_company_name=(name)
    @owned_tenant_company_name = name
    if !self.is_invited_and_registering_first_time
      self.owned_tenant_company = TenantCompany.new(:name=>name)
    end
  end

  def owned_tenant_company_name
    @owned_tenant_company_name
  end  

  def assign_moderator_role_if_owner_of_company
    # check was the user ever invited
    if self.invited_by.blank? # this guy was never invited so assume he has created the company if it exists
      if self.owned_tenant_company == nil  then
        self.add_role :user
        self.save
      else
        self.add_role :owner
        self.tenant_company = self.owned_tenant_company
        self.save
      end
    end
  end

  def create_the_tenant_if_company_name_provided
    # check is the user already associated with a tenant
    if self.tenant_company == nil || self.invited_by.blank? then # no tenant
      if !self.owned_tenant_company_name.blank? then
        self.owned_tenant_company.save
      end
    else
      self.owned_tenant_company = nil
    end

  end

  def remove_tenant_if_company_name_not_provided
    if self.owned_tenant_company_name.blank? || self.is_invited_and_registering_first_time then
      self.owned_tenant_company = nil
    end
  end

  def role_name
    role = self.roles.first
    if role!=nil then
      role.name
    else
      ''
    end
  end

  scope :accessible_records, lambda {|usr| 
    if !usr.has_role? :admin
      joins(:tenant_user).where('tenant_users.tenant_company_id = ?', usr.tenant_company.id)  
    end
  } 

  scope :by_name, lambda{|search| where('name LIKE ?', search.to_s+'%')}
  scope :not_my_collegues, lambda{|usr| 
          joins(:tenant_user).where('tenant_users.tenant_company_id <> ?', usr.tenant_company.id)  
        }
end
