class ContactDetail < ActiveRecord::Base
  belongs_to :contactable, :polymorphic => true
  attr_accessible :contactable, :details, :location, :type
end
