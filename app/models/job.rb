class Job < ActiveRecord::Base
  attr_accessible :name, :description, :customer_name, :tenant_company_id, :customer_id, :customer_type
  belongs_to :tenant_company
  belongs_to :customer, :polymorphic => true

  def customer_name=(name)
   @customer_name = name
  end
    
  def customer_name()
	  if self.customer_id!= nil then
	    self.customer_name = self.customer.name
	  end
	  @customer_name
  end

  def belongs_to_current_user_company?(usr)
  	if usr.tenant_company == nil then
  		false
		else
	  	self.tenant_company == usr.tenant_company
  	end
  end

  scope :by_tenant_company, lambda{|tenant_company| where(:tenant_company_id=> tenant_company.id)}
  scope :by_customer, lambda{|customer| where('customer_id = ? AND customer_type = ?', customer.id, customer.class.name)}
  scope :not_by_tenant_company, lambda{|tenant_company| where(Job.arel_table[:tenant_company_id].not_eq(tenant_company.id))}
  scope :not_by_customer, lambda{|customer| where('customer_id <> ? AND customer_type <> ?', customer.id, customer.class.name)}
  
end
