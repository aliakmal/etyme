class DirectoryController < ApplicationController
  def companies
    @companies = TenantCompany.page(params[:page])

  end

  def listings
  	companies = TenantCompany.by_name(params[:term]).not_my_company(current_user)
  	users = User.by_name(params[:term]).not_my_collegues(current_user)

	results = (companies+users)
	results = results.sort_by { |hsh| hsh[:created_at] }
    results = results.map{|hsh| { :id => hsh.id, :name => hsh.name, :type=>hsh.class.name, :details=>(hsh.class.name=='TenantCompany'? '': (!hsh.actual_current_tenant_company_name ? '':hsh.actual_current_tenant_company_name) ) }}
	respond_to do |format|
      format.json {render :json => results.as_json() }
    end

  end

  def individuals
    @users = User.page(params[:page])
  end
end
