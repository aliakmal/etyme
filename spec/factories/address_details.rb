# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address_detail, :class => 'AddressDetails' do
    address "MyText"
    city "MyString"
    country "MyString"
    state "MyString"
    zip "MyString"
    address_id 1
  end
end
