class CreateTenantCompanies < ActiveRecord::Migration
  def change
    create_table :tenant_companies do |t|
      t.string :name
      t.text :about

      t.timestamps
    end
  end
end
