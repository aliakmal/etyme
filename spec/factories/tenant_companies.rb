# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tenant_company do
    name "MyString"
    about "MyText"
  end
end
