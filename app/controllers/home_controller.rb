class HomeController < ApplicationController
  def index
  	if user_signed_in? then
      
      redirect_to dashboard_path
    else
      redirect_to new_user_registration_path
    end
  end
  def dashboard
  	# if the user has no company tenant render the no tenant dashboard
  	if current_user.belongs_to_a_company?
  	# if the user has a company render the tenant dashboard
  		@jobs_my_company = Job.by_tenant_company(current_user.tenant_company) # get jobs your company has created
  		@jobs_im_customer = Job.by_customer(current_user.tenant_company) + Job.by_customer(current_user) # get jobs your company has created
  		@jobs_others = Job.not_by_tenant_company(current_user.tenant_company).not_by_customer(current_user).not_by_customer(current_user.tenant_company)
  		render :action => 'dashboard_tenant'
  	else
  		render :action => 'dashboard_no_tenant'
	  end
  end

  def dashboard_no_tenant

  end

  def dashboard_tenant

  end
end
