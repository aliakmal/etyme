module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end

  
  def sortable(column, title = nil)  
    title ||= column.titleize  
    css_class = column == params[:sort] ? "current sorting_#{sort_direction}" : nil  
    direction = column == params[:sort] && sort_direction == "asc" ? "desc" : "asc"  
    if direction == 'asc' then
      html = <<-HTML
      #{title}
   
      <i class="icon-circle-arrow-up"></i>
      HTML
    else
      html = <<-HTML
      #{title}
      <i class="icon-circle-arrow-down"></i>
      HTML
    end
    
    prms = {}
    
    params.each do |index, value|
      prms[index] = value
    end
    
    prms['sort'] = column
    prms['direction'] = direction
    
    link_to raw(html), prms, {:class => css_class}
  end
    
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  
  def link_to_add_contact_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contact_details/'+association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :style=>'margin-top:6px; display:inline-block;')
    
  end
  
  def link_to_add_address_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    new_object.build_address_detail

    #new_object[:type] = association
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render("contact_details/address_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :style=>'margin-top:6px; display:inline-block;')
  end

def application_environment
    if request.host == 'lvh.me' || request.host == 'localhost' then
      'localhost'
    elsif request.host == 'test.fltctr.com' then
      'test server'
    else
      false
    end
  end

  # For generating time tags calculated using jquery.timeago
  def timeago(time, options = {})
    options[:class] ||= "timeago"
    content_tag(:abbr, time.to_s, options.merge(:title => time.getutc.iso8601)) if time
  end

  # Shortcut for outputing proper ownership of objects,
  # depending on who is looking
  def whose?(user, object)
    case object
      when Post
        owner = object.author
      when Comment
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        "his"
      else
        "#{owner.name}'s"
      end
    else
      ""
    end
  end

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type)
    if object
      link_to object.to_string, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end
end
