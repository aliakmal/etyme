class TenantCompany < ActiveRecord::Base
  attr_accessible :about, :name

  attr_accessible :contact_detail_attributes, :phone_attributes, :fax_attributes, :emails_attributes, :im_attributes
  attr_accessible :address_detail_attributes, :addresses_attributes

  belongs_to :owner, :class_name => 'User'
  has_many :jobs

  has_many :assignments, :class_name => 'Job', :as => :customer

  has_many :contact_details, :as => :contactable
  has_many :tenant_users
  has_many :users, :through=>:tenant_users

  has_many :phone, :as => :contactable
  has_many :fax, :as => :contactable
  has_many :emails, :as => :contactable
  has_many :im, :as => :contactable
  has_many :addresses, :as => :contactable

  accepts_nested_attributes_for :phone, :fax, :emails, :addresses, :im
  accepts_nested_attributes_for :contact_details


  def first_phone
    if self.phone.count > 0 then
      self.phone.first.details
    else
      ''
    end
  end
  
  def second_phone
    if self.phone.count > 1 then
      self.phone[1].details
    else
      ''
    end
  end
  
  def third_phone
    if self.phone.count > 2 then
      self.phone[2].details
    else
      ''
    end
  end

  def first_fax
    if self.fax.count > 0 then
      self.fax.first.details
    else
      ''
    end
  end

  def first_email
    if self.email_address.count > 0 then
      self.email_address.first.details
    else
      ''
    end
  end
  
  def second_email
    if self.email_address.count > 1 then
      self.email_address[1].details
    else
      ''
    end
  end
  
  def third_email
    if self.email_address.count > 2 then
      self.email_address[2].details
    else
      ''
    end
  end
  
  def first_address
    if self.addresses.count > 0 then
      address = self.addresses.first
    else
      ''
    end
  end
  
  def second_address
    if self.addresses.count > 1 then
      address = self.addresses[1]
    else
      ''
    end
  end

  def third_address
    if self.addresses.count > 2 then
      address = self.addresses[2]
    else
      ''
    end
  end
  
  def first_im
    if self.im.count > 0 then
      self.im.first.details
    else
      ''
    end
  end
  
  def second_im
    if self.im.count > 1 then
      self.im[1].details
    else
      ''
    end
  end
  
  def third_im
    if self.im.count > 2 then
      self.im[2].details
    else
      ''
    end
  end
  def first_im_kind
    if self.im.count > 0 then
      self.im.first.location
    else
      ''
    end
  end
  
  def second_im_kind
    if self.im.count > 1 then
      self.im[1].location
    else
      ''
    end
  end
  
  def third_im_kind
    if self.im.count > 2 then
      self.im[2].location
    else
      ''
    end
  end
  
  def to_string
    self.name
  end


  scope :by_name, lambda{|search| where('name LIKE ?', search.to_s+'%')}
  scope :not_my_company, lambda{|usr| 
          where('id <> ?', usr.tenant_company.id)  
        }


end
