class TenantUser < ActiveRecord::Base
	belongs_to :tenant_company
	belongs_to :user
	attr_accessible :user_id, :tenant_company_id
end