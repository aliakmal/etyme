# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_detail do
    contactable_type "MyString"
    contactable_id 1
    type ""
    details "MyText"
    location "MyString"
  end
end
