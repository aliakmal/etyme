class CreateAddressDetails < ActiveRecord::Migration
  def change
    create_table :address_details do |t|
      t.text :address
      t.string :city
      t.string :country
      t.string :state
      t.string :zip
      t.integer :address_id

      t.timestamps
    end
  end
end
