class Users::InvitationsController < Devise::InvitationsController
    # GET /resource/invitation/new
  def new
    build_resource
    render 'users/invitations/invitations/new'
  end

  # POST /resource/invitation
  def create
    self.resource = resource_class.invite!(resource_params, current_inviter)

    if resource.errors.empty?
      set_flash_message :notice, :send_instructions, :email => self.resource.email
	
	    self.resource.tenant_company = current_user.tenant_company
	    self.resource.save

      respond_with resource, :location => after_invite_path_for(resource)
    else
      respond_with_navigational(resource) {     render 'users/invitations/invitations/new' }
    end
  end

  # GET /resource/invitation/accept?invitation_token=abcdef
  def edit
    render :edit
  end
end