require "spec_helper"

describe TenantCompaniesController do
  describe "routing" do

    it "routes to #index" do
      get("/tenant_companies").should route_to("tenant_companies#index")
    end

    it "routes to #new" do
      get("/tenant_companies/new").should route_to("tenant_companies#new")
    end

    it "routes to #show" do
      get("/tenant_companies/1").should route_to("tenant_companies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/tenant_companies/1/edit").should route_to("tenant_companies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/tenant_companies").should route_to("tenant_companies#create")
    end

    it "routes to #update" do
      put("/tenant_companies/1").should route_to("tenant_companies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/tenant_companies/1").should route_to("tenant_companies#destroy", :id => "1")
    end

  end
end
