require 'spec_helper'

describe DirectoryController do

  describe "GET 'companies'" do
    it "returns http success" do
      get 'companies'
      response.should be_success
    end
  end

  describe "GET 'individuals'" do
    it "returns http success" do
      get 'individuals'
      response.should be_success
    end
  end

end
