class AddressDetail < ActiveRecord::Base
  attr_accessible :address, :address_id, :city, :country, :state, :zip
  belongs_to :address, :foreign_key => :address_id
end
