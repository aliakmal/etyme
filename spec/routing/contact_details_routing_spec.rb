require "spec_helper"

describe ContactDetailsController do
  describe "routing" do

    it "routes to #index" do
      get("/contact_details").should route_to("contact_details#index")
    end

    it "routes to #new" do
      get("/contact_details/new").should route_to("contact_details#new")
    end

    it "routes to #show" do
      get("/contact_details/1").should route_to("contact_details#show", :id => "1")
    end

    it "routes to #edit" do
      get("/contact_details/1/edit").should route_to("contact_details#edit", :id => "1")
    end

    it "routes to #create" do
      post("/contact_details").should route_to("contact_details#create")
    end

    it "routes to #update" do
      put("/contact_details/1").should route_to("contact_details#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/contact_details/1").should route_to("contact_details#destroy", :id => "1")
    end

  end
end
