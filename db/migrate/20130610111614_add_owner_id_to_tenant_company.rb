class AddOwnerIdToTenantCompany < ActiveRecord::Migration
  def change
  	add_column :tenant_companies, :owner_id, :integer
  end
end
