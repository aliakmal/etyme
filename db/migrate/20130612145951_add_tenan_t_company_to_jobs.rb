class AddTenanTCompanyToJobs < ActiveRecord::Migration
  def change
  	change_table :jobs do |t|
      t.references :tenant_company
    end
  end
end
