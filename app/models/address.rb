class Address < ContactDetail
    has_one :address_detail
    attr_accessible :address_detail_attributes, :address, :city, :zip, :state, :country
    accepts_nested_attributes_for :address_detail

    def address=(name)
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:address] = name
    end
    
    def address()
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:address]
    end

    def city=(name)
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:city] = name
    end
    
    def city
      if self.address_detail == nil then
        self.build_address_detail
      end

      self.address_detail[:city]
    end

    def zip=(name)
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:zip] = name
    end
    
    def zip
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:zip]
    end

    def state=(name)
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:state] = name
    end
    
    def state
      if self.address_detail == nil then
        self.build_address_detail
      end
      
      self.address_detail[:state]
    end
    
    def country=(name)
      if self.address_detail == nil then
        self.build_address_detail
      end
      
      self.address_detail[:country] = name
    end
    
    def country()
      if self.address_detail == nil then
        self.build_address_detail
      end

      self.address_detail[:country]
    end
    
    #after_save :assign_tuple
    
    def assign_tuple
      if self.address_detail == nil then
        self.build_address_detail
      end
      self.address_detail[:address] = self.address
      self.address_detail[:city] = self.city
      self.address_detail[:country] = self.country
      self.address_detail[:state] = self.state
      self.address_detail[:zip] = self.zip
      #raise self.address_detail.inspect
      self.address_detail.save
    end

end