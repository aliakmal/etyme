class CreateContactDetails < ActiveRecord::Migration
  def change
    create_table :contact_details do |t|
      t.references :contactable, :polymorphic => true
      #t.string :contactable_type
      #t.integer :contactable_id
      t.string :type
      t.text :details
      t.string :location

      t.timestamps
    end
  end
end
