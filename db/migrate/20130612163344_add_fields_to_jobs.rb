class AddFieldsToJobs < ActiveRecord::Migration
  def change
  	add_column :jobs, :city, :string
  	add_column :jobs, :state, :string
  	add_column :jobs, :zip, :string

  	add_column :jobs, :start_date, :date
  	add_column :jobs, :salary, :string
  	add_column :jobs, :duration, :string
  	add_column :jobs, :type, :string
  	add_column :jobs, :openings, :integer
  	add_column :jobs, :remaining_openings, :integer
  	add_column :jobs, :company_job_id, :string
  	add_column :jobs, :category, :string
  end
end
