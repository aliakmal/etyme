class ListItem < ActiveRecord::Base
  # This method call is needed to configure the model correctly
  acts_as_listable_view
end