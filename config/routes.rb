Etyme::Application.routes.draw do

  resources :jobs

  match "directory/companies" => "directory#companies", :as=>:directory_companies, :via=>:get
  match "directory/listings" => "directory#listings", :as=>:directory_listings, :via=>:get

  match "directory/individuals" => "directory#individuals", :as=>:directory_individuals, :via=>:get

  match "dashboard" => "home#dashboard", :as=>:dasboard, :via=>:get
  match "dashboard_no_tenant" => "home#dashboard_no_tenant", :as=>:dasboard_no_tenant, :via=>:get
  match "dashboard_tenant" => "home#dashboard_tenant", :as=>:dasboard_tenant, :via=>:get  
  resources :contact_details

  resources :tenant_companies

  authenticated :user do
    root :to => 'home#dashboard'
  end
  root :to => "home#index"
  match "/users/:id/edit_user" => "users#edit_user", :as=>:edit_the_user, :via=>:get
  devise_for :users, :controllers => { :invitations => 'users/invitations' }
  match "/users/:id/update_user" => "users#update_user", :as=>:update_the_user, :via=>:put
  resources :users
end