class AddCUstomerToJobs < ActiveRecord::Migration
  def change
  	change_table :jobs do |t|
      t.references :customer, :polymorphic => true
    end
  end
end
