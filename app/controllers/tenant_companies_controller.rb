class TenantCompaniesController < ApplicationController
  # GET /tenant_companies
  # GET /tenant_companies.json
  def index
    @tenant_companies = TenantCompany.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tenant_companies }
    end
  end

  # GET /tenant_companies/1
  # GET /tenant_companies/1.json
  def show
    @tenant_company = TenantCompany.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tenant_company }
    end
  end

  # GET /tenant_companies/new
  # GET /tenant_companies/new.json
  def new
    @tenant_company = TenantCompany.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tenant_company }
    end
  end

  # GET /tenant_companies/1/edit
  def edit
    @tenant_company = TenantCompany.find(params[:id])
  end

  # POST /tenant_companies
  # POST /tenant_companies.json
  def create
    @tenant_company = TenantCompany.new(params[:tenant_company])

    respond_to do |format|
      if @tenant_company.save
        format.html { redirect_to @tenant_company, notice: 'Tenant company was successfully created.' }
        format.json { render json: @tenant_company, status: :created, location: @tenant_company }
      else
        format.html { render action: "new" }
        format.json { render json: @tenant_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tenant_companies/1
  # PUT /tenant_companies/1.json
  def update
    @tenant_company = TenantCompany.find(params[:id])

    respond_to do |format|
      if @tenant_company.update_attributes(params[:tenant_company])
        format.html { redirect_to @tenant_company, notice: 'Tenant company was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tenant_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tenant_companies/1
  # DELETE /tenant_companies/1.json
  def destroy
    @tenant_company = TenantCompany.find(params[:id])
    @tenant_company.destroy

    respond_to do |format|
      format.html { redirect_to tenant_companies_url }
      format.json { head :no_content }
    end
  end
end
